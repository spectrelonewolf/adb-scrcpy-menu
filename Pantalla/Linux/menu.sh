#!/bin/bash

# Función para realizar el emparejamiento
emparejar() {
    echo "Ingrese la IP:"
    read ip
    echo "Ingrese el puerto de emparejamiento:"
    read puerto_emparejamiento
    echo "Ingrese el PIN:"
    read pin

    adb pair $ip:$puerto_emparejamiento $pin
}

# Función para conectar mediante adb
conectar() {
    echo "Ingrese la IP:"
    read ip
    echo "Ingrese el puerto de conexión:"
    read puerto_conexion

    adb connect $ip:$puerto_conexion
}

# Función para visualizar usando scrcpy
visualizar() {
    echo "Ingrese la IP:"
    read ip
    echo "Ingrese el puerto para scrcpy:"
    read puerto_scrcpy

    scrcpy --tcpip=$ip:$puerto_scrcpy
}

# Menú principal
while true; do
    echo "Seleccione una opción:"
    echo "1. Emparejar dispositivo (adb pair)"
    echo "2. Conectar dispositivo (adb connect)"
    echo "3. Visualizar con scrcpy"
    echo "4. Salir"

    read opcion

    case $opcion in
        1)
            emparejar
            ;;
        2)
            conectar
            ;;
        3)
            visualizar
            ;;
        4)
            echo "Saliendo del programa."
            exit 0
            ;;
        *)
            echo "Opción inválida. Por favor, seleccione una opción del 1 al 4."
            ;;
    esac

    echo ""  # Salto de línea para mejor legibilidad
done
