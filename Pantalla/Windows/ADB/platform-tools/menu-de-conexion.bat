@echo off
:menu
cls
echo ====================================================================================      
echo Menu de conexion para presentacion - Cypher Vault
echo ====================================================================================
echo 0- conexion por USB
echo 1- Emparejamiento
echo 2- Conexion
echo 3- Visualizacion
echo 4- Salir 
echo ==================
set /p choice="Seleccione una opcion (0-4): "

if %choice%==0 goto visualizacion_usb
if %choice%==1 goto emparejamiento
if %choice%==2 goto conexion
if %choice%==3 goto visualizacion
if %choice%==4 goto salir
echo Opcion no valida. Intente de nuevo.
pause
goto menu

:visualizacion_usb
set /p resolucion="Ingrese la resolucion maxima de pantalla (ej: 400,720,1280,2000)"
.\scrcpy --video-codec=h265 --max-size="%resolucion%" --max-fps=60 --no-audio --keyboard=uhid
pause
goto menu

:emparejamiento
set /p ip="Ingrese la IP: "
set /p socket="Ingrese el puerto: "
set /p codigo="Ingrese el pin: "
.\adb pair %ip%:%socket% %codigo%
pause
goto menu

:conexion
set /p ip="Ingrese la IP: "
set /p socket="Ingrese el puerto: "
.\adb connect %ip%:%socket%
pause
goto menu

:visualizacion
set /p ip="Ingrese la IP: "
set /p socket="Ingrese el puerto: "
.\scrcpy --tcpip=%ip%:%socket%
pause
goto menu

:salir
exit